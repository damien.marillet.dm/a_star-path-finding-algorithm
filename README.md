Implementation of A* path finding algorithm in Python, using pygame

Implementation inspired by YouTube tutorial:
https://www.youtube.com/watch?v=JtiK0DOeI4A

How to use it:

The configurable parameters ( size of the window, number of rows ) can be changed at the beginning of the Display.py file
Then just run the Display.py file to open the window.
The first spot to be clicked on will be the Starting point of the A* algorithm
The second spot to be clicked on will be the Goal point of the A* algorithm
Then all other click will create a barrier/obstacle.

To reset the grid press the R key.
To launch the path finding algorithm ( when a Goal and Starting point has been added ) press the SpaceBar.

Possible extensions:
- Using better heuristics, with the help of this article http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
- Using A star for Goal Oriented Action Planning 

Demo gif from the project:

![image alternative text](/Demo.gif)
