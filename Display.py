import pygame
from pygame.constants import KEYDOWN
import pygame.event
import pygame.mouse
import pygame.display
import pygame.draw
from A_star import A_star, manhattan_heuristic

WIDTH = 600 #width of the display window
WINDOW = pygame.display.set_mode((WIDTH,WIDTH))
ROWS=40 #number of rows in the grid
pygame.display.set_caption("A Star Path Finding Algorithm")

WHITE=(255,255,255)
BLACK=(0,0,0)
YELLOW=(255,255,0)
RED=(255,0,0)
GREY=(128,128,128)
GREEN=(0,255,0)
BLUE=(0,0,255)
PURPLE = (128, 0, 128)




def create_grid(rows, width):
    grid=[]
    gap=width//rows
    for i in range(rows):
        grid.append([])
        for j in range(rows):
            s=Spot(i*gap,j*gap,i,j,gap,rows)
            grid[i].append(s)   
    return grid

def draw_grid_lines(window,rows,width):
    gap= width//rows
    for i in range(rows+1):
        pygame.draw.line(window,color=GREY,start_pos=(0,i*gap),end_pos=(width,i*gap))
        for j in range(rows):
            pygame.draw.line(window,color=GREY,start_pos=(j*gap,0),end_pos=(j*gap,width))

def draw(window,grid,rows,width):
    #Draw all elements for one frame
    window.fill(WHITE)
    for row in grid:
        for spot in row: 
            spot.draw(window)
    draw_grid_lines(window,rows,width)
    pygame.display.update()

def get_grid_pos(pos,rows,width):
    x,y=pos
    gap=width//rows
    row=x//gap
    col=y//gap
    return row,col


class Spot:
    def __init__(self, x, y, row, col, size , total_rows):
        self.x=x
        self.y=y
        self.row = row
        self.col=col
        self.size=size
        self.total_rows=total_rows
        self.color=WHITE
        self.neighbors=[]
    
    def get_pos(self):
        return self.row,self.col
    #the make_... functions change spot color according to its status
    def make_open(self):
        self.color=RED
    def make_start(self):
        self.color=GREEN
    def make_barrier(self):
        self.color=BLACK
    def make_end(self):
        self.color=BLUE
    def make_closed(self):
        self.color=YELLOW
    def make_pathway(self):
        self.color=PURPLE
    #functions to check the spot state
    def is_barrier(self):
        return self.color==BLACK

    def update_neighbors(self, grid):
        #update what spot is accessible from that spot
        neighbors=[]
        N=self.total_rows
        for i in range(3):
            for j in range(3):
                #to avoid setting itself as a neighbor
                if i==j==1 :
                    continue
                if self.row-1+i<0 or self.col-1+j<0 or self.row-1+i>=N or self.col-1+j>=N :#to check if its not out of the grid
                    continue
                neighbor=grid [self.row-1+i][self.col-1+j]
                if neighbor.is_barrier():
                    continue
                neighbors.append(neighbor)

        self.neighbors=neighbors
    
    #when comparing two Spot object
    def __lt__(self, other):
        return False

    def draw(self, window):
        pygame.draw.rect(window, self.color, (self.x,self.y,self.size,self.size) )


def main(window,width,rows):
    
    grid=create_grid(ROWS,width)
    start_spot=None
    goal_spot=None
    is_running=True
    while is_running:
        draw(window,grid,ROWS,width)
        for event in pygame.event.get(): # list of all event happening that frame ( example : a key has been pressed)
            if event.type == pygame.QUIT:
                is_running=False
            

            if pygame.mouse.get_pressed()[0]: # the [0] is to get information for left click only
                pos=pygame.mouse.get_pos()
                row,col=get_grid_pos(pos,ROWS,width)
                spot= grid[row][col]
                #lets manually set up our map ! with a start and end pos, and some obstacles/barriers
                if not start_spot: 
                    start_spot=spot
                    start_spot.make_start()
                elif not goal_spot:
                    goal_spot=spot
                    goal_spot.make_end()
                elif spot != goal_spot and spot != start_spot:
                    spot.make_barrier()

            if event.type == pygame.KEYDOWN: #the user launch the algorithm with space bar
                if event.key == pygame.K_SPACE and start_spot and goal_spot:
                    for row in grid:
                        for spot in row:
                            spot.update_neighbors(grid)
                    A_star( lambda:draw(window,grid,ROWS,width),manhattan_heuristic, grid, start_spot, goal_spot)
                if event.key == pygame.K_r :
                    start_spot=None
                    goal_spot=None
                    grid=create_grid(ROWS,width)                
                
    pygame.quit()
    
main(WINDOW,WIDTH,ROWS)