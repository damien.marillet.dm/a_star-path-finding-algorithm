import math
from queue import PriorityQueue

def A_star(draw_function, h, grid, start, goal):
    #initialization
    open_spots=PriorityQueue()
    closed_spots={}
    open_spots.put((0,start))
    #the scores are stocked in a table and float("inf") is an unbounded upper value for comparison ( for init )
    g_score={ spot: float("inf") for row in grid for spot in row}
    f_score={ spot: float("inf") for row in grid for spot in row}
    g_score[start]=0
    f_score[start]= h(start.get_pos(),goal.get_pos())
    came_from={}
    open_spots_set={start}

    while not open_spots.empty():
        #find and get the node with the least f in open spots
        q=open_spots.get()[1]
        open_spots_set.remove(q)


        #generate q's 8 successors and set their parent to q
        for succ in q.neighbors:

            #check if this path is better than before
            g_tmp=g_score[q]+1
            f_tmp=g_score[q]+h(succ.get_pos(),goal.get_pos())
            if g_tmp<g_score[succ]:
                came_from[succ]=q
                g_score[succ]=g_tmp
                f_score[succ]=f_tmp
                if succ not in open_spots_set:
                    open_spots.put((f_score[succ],succ))
                    open_spots_set.add(succ)
                    came_from[succ]=q
                    succ.make_open()

            if succ==goal: #GOAL FOUND
                reconstruct_path(draw_function,came_from,succ)
                return True
        
        draw_function()

        #add to closed set
        if q!=start:
           q.make_closed()

        
def reconstruct_path(draw_function,came_from,current):
    while current in came_from: # works because start has NO parent in came_from
        current=came_from[current]
        current.make_pathway()
        draw_function()



#A GUIDE TO HEURISTICS : http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
def manhattan_heuristic(p1,p2):
    x1,y1=p1
    x2,y2=p2
    return math.sqrt ( (x1-x2 )**2 + (y1-y2)**2)
     
